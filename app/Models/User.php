<?php

namespace App\Models;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable;

    protected $fillable = [
        'name', 'username', 'password', 'role_id',
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function role()
    {
        return $this->hasOne(Role::class);
    }

    public function products() {

        return $this->belongsToMany(Product::class, 'user_products');
    }

    public function hasRole($id)
    {
        return $this->role_id == $id;
    }

    public function isAdmin()
    {
        return $this->hasRole(Role::ADMIN);
    }

    public function isUser()
    {
        return $this->hasRole(Role::USER);
    }
}
